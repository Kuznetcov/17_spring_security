<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<meta charset="utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">

</head>

<body>
	<div class="wrapper">


		<div class="b-page__line">
			<div class="b-menu">
				<a href="places" class='b-link b-link_menu b-link_menu_active'>HOME</a>
				<a href="#" class='b-link b-link_menu'>ABOUT US</a> <a
					href="agreement" class='b-link b-link_menu'>SERVICES</a> <a
					href="#" class='b-link b-link_menu'>CUSTOMERS</a> <a href="#"
					class='b-link b-link_menu'>CONTACT</a>
			</div>
		</div>

		<div class="middle">

			<div class="container" id="1234">
				<main class="content" id="mainClass">
				<div id="popup" style="display: block">

					<div class="info" id="sendform" style="display: block">
						<span class="popup-close" id='close'>x</span>
						
						
						<form:form action="/mvcbased/registration" method="post" commandName="userRegistration">
								<table>
									<tr>
										<td colspan="2" align="center"><h2>Registration</h2></td>
									</tr>
									<tr>
										<td>Username</td>
										<td><form:input path="username" /></td>
									</tr>
									<tr>
										<td>Login</td>
										<td><form:input path="login" /></td>
									</tr>
									<tr>
										<td>Password:</td>
										<td><form:password path="password" /></td>
									</tr>
									<tr>
										<td>Birthdate</td>
										<td><form:input type="date" path="birthdate" /></td>
									</tr>
									<tr>
										<td>Email</td>
										<td><form:input path="email" /></td>
									</tr>
									<tr>
										<td colspan="2" align="center">
										<input type="submit" value="Register" /></td>
									</tr>
								</table>
							</form:form>

					</div>
				</div>
			</div>
			<!-- .container-->

		</div>
		<!-- .middle-->

	</div>
	<!-- .wrapper -->

	<script src="${pageContext.request.contextPath}/js/popup.js" async></script>
	<script src="${pageContext.request.contextPath}/js/jquery-3.0.0.js"></script>
</body>
</html>