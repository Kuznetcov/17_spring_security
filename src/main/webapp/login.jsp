<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<meta charset="utf-8" />
<title>Login page</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">

</head>

<body>
	<div class="wrapper">


		<div class="b-page__line">
			<div class="b-menu">
				<a href="places" class='b-link b-link_menu b-link_menu_active'>HOME</a>
				<a href="#" class='b-link b-link_menu'>ABOUT US</a> <a
					href="agreement" class='b-link b-link_menu'>SERVICES</a> <a
					href="#" class='b-link b-link_menu'>CUSTOMERS</a> <a href="#"
					class='b-link b-link_menu'>CONTACT</a>
			</div>
		</div>

		<div class="middle">

			<div class="container" id="1234">
				<main class="content" id="mainClass">
				<div id="popup" style="display: block">

					<div class="info" id="sendform" style="display: block">
						
						<form name="f" method="post">
						<c:if test="${param.logout != null}">
                                <div class="alert alert-success">
                                    <p>You have been logged out successfully.</p>
                                </div>
                        </c:if>
                        <c:if test="${param.error != null}">
                                <div class="alert alert-success">
                                    <p>User not found</p>
                                </div>
                        </c:if>
							<form:form action="login" method="post" commandName="userForm">
								<table>
									<tr>
										<td colspan="2" align="center"><h2>Login</h2></td>
									</tr>
									<tr>
										<td>Login:</td>
										<td><form:input path="login" /></td>
									</tr>
									<tr>
										<td>Password:</td>
										<td><form:password path="password" /></td>
									</tr>
									<tr>
										<td>Remember me:</td>
                                		<td><input type="checkbox" id="rememberme" name="remember-me"/></td>
									</tr>
									<tr>
										<td colspan="2" align="center">
										<input type="submit" value="Login" /></td>
									</tr>
								</table>
							</form:form>

							<!-- 							Login:<br> <input type="text" name="login"
								pattern="[a-zA-Z]{4,}" required><br> Password:<br>
							<input type="text" name="password" pattern="[a-zA-Z]{4,}"
								required><br> <input type="submit" /> -->
						</form>

						<br>
						Registration: <a href="/mvcbased/registration">link</a>
					</div>
				</div>
			</div>
			<!-- .container-->

		</div>
		<!-- .middle-->

	</div>
	<!-- .wrapper -->
</body>
</html>