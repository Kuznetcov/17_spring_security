package com.controller;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.dal.ModelComposer;
import com.data.Roles;
import com.data.User;

@Controller
@RequestMapping("/registration")
public class Registration {

  private Logger log = LoggerFactory.getLogger(Registration.class);
  
  @Autowired
  private ModelComposer modelComposer;

  @RequestMapping(method = RequestMethod.GET)
  public String showLogin(ModelMap model) {
    User userForm = new User();    
    model.put("userRegistration", userForm);
    return "registration";
  }

  @RequestMapping(method = RequestMethod.POST)
  public String checkLoginForm(ModelMap model, @ModelAttribute("userRegistration") User user, HttpServletRequest request,
      HttpServletResponse response) {
    Set<Roles> roles = new HashSet<Roles>();
    roles.add(new Roles(1,"REGISTERED_USER"));
    user.setRoles(roles);
    modelComposer.addUser(user);
    log.info("User added: "+user);
    return "redirect:login";
  }

}
