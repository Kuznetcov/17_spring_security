package com.dal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import com.data.ParkingPlace;
import com.data.PlaceEvent;
import com.data.User;

public class ModelComposerImpl implements ModelComposer {

  @Autowired
  RestTemplate restTemplate;
  
  @Autowired
  private PasswordEncoder passwordEncoder;
  
  @Override
  @Cacheable("places")
  public List<ParkingPlace> getParkingPlaces() {
    ParkingPlace[] list = restTemplate.getForObject("http://localhost:8080/soap-api/rest/service/places", ParkingPlace[].class);
    return new ArrayList<ParkingPlace>(Arrays.asList(list));
  }

  @Override
  public String addUser(String name, String login, String password) {
    User request = new User(name,login,password);
    restTemplate.put("http://localhost:8080/soap-api/rest/service/newuser", request);
    return name;
  }

  @Override
  public String getUser(String login, String password) {
    User user = restTemplate.getForObject("http://localhost:8080/soap-api/rest/service/{login}/{password}", User.class,login,password);
    return user.getUsername(); 
  }

  @Override
  @CacheEvict(value = "places", allEntries=true)
  public String addEvent(PlaceEvent event) {
    restTemplate.put("http://localhost:8080/soap-api/rest/service/placeevent", event);
    return event.placeID.toString();
  }

  @Override
  public List<ParkingPlace> getParkingPlacesByPage(Integer pageNumber) {
    String urlParam= "";
    if (pageNumber==0){
      urlParam="?from=0&max=10";
    } else {
      urlParam="?from="+((pageNumber*10)-10)+"&max=10";
    }
    ParkingPlace[] list = restTemplate.getForObject("http://localhost:8080/soap-api/rest/service/placeswithpaging"+urlParam, ParkingPlace[].class);
    return new ArrayList<ParkingPlace>(Arrays.asList(list));
  }

  @Override
  public User getUserByLogin(String login) {
    User user = restTemplate.getForObject("http://localhost:8080/soap-api/rest/service/{login}", User.class,login);
    return user;
  }

  @Override
  public String addUser(User user) {
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    restTemplate.put("http://localhost:8080/soap-api/rest/service/newuserwithrole", user);
    return null;
  }

  @Override
  @CacheEvict(value = "places", allEntries=true)
  public String addParkingPlace(ParkingPlace place) {
    restTemplate.put("http://localhost:8080/soap-api/rest/service/parkingplace/add", place);
    return null;
  }

  @Override
  @CacheEvict(value = "places", allEntries=true)
  public String deleteParingPlace(Integer id) {
    restTemplate.delete("http://localhost:8080/soap-api/rest/service/parkingplace/delete/{placeid}",id);
    return null;
  }

}
