package com.dal;

import java.util.List;

import com.data.ParkingPlace;
import com.data.PlaceEvent;
import com.data.User;

public interface ModelComposer {

  public List<ParkingPlace> getParkingPlaces();

  public String addUser(String name, String login, String password);
  
  public String getUser(String login, String password);
  
  public User getUserByLogin(String login);
  
  public String addEvent(PlaceEvent event);
  
  public List<ParkingPlace> getParkingPlacesByPage(Integer pageNumber);
  
  public String addUser(User user);
  
  public String addParkingPlace(ParkingPlace place);
  
  public String deleteParingPlace(Integer id);
  
}
